Binární soubor spustitelný pod linux-x64 je ve složce build. Vstupní parametry jsou soubor s jednou větou na každém řádku. Druhý parametr zadat '2', protože úloha řeší bigramy.
Výstup z aplikace je EN_bigram.csv a CZ_bigram.csv. Vygenerované csv jsou z SEN\*.txt.onespace.
Protože ve výstupu se řeší kartézký součin všech unikátních slov, tak vevyskytující slova s nějakým předchozí slovem jsou označeny '\*'.
Přiklad běhů | * | 2.74718e-10

Použité přikazy SRILM. Český text jsem musel uměle ořezat po 100 slovech, protože se v něm nacházelo pár výskytů vět s více jak 100 slovy.
```
  ./ngram-count -text /tmp/out.txt -order 2 -write /tmp/cz.count -wbdiscount
  ./ngram-count -read /tmp/cz.count -order 2 -lm /tmp/cz.lm -wbdiscount
  ./ngram-count -text /home/daniel/pro/pli/wb/SENEN.txt.onespace -order 2 -write /tmp/en.count -wbdiscount
  ./ngram-count -read /tmp/en.count -order 2 -lm /tmp/en.lm -wbdiscount
```
Výstup SRILM je ve složce SRILM
