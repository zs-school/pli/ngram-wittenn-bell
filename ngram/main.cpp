#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <cstdio>
#include <map>
#include <algorithm>
#include <cmath>

using namespace std;

size_t split(const std::string &txt, std::vector<std::string> &strs, char ch)
{
    size_t pos = txt.find( ch );
    size_t initialPos = 0;
    strs.clear();

    // Decompose statement
    while( pos != std::string::npos ) {
        strs.push_back( txt.substr( initialPos, pos - initialPos ) );
        initialPos = pos + 1;

        pos = txt.find( ch, initialPos );
    }

    // Add the last one
    strs.push_back( txt.substr( initialPos, std::min( pos, txt.size() ) - initialPos + 1 ) );

    return strs.size();
}

int main(int argc, char ** argv)
{
    int n = 0;
    string filepath;
    if (argc == 1)
    {
        cout << "Enter filepath: (enter to continue)" << endl;
        cin >> filepath;
        cout << "Enter ngram (number): (enter to continue)" << endl;
        cin >> n;
    }
    else
    {
        if (argc < 3)
        {
            cout << "Usage: ./<program_name><path_to_file><n gram>" << endl;
            return -1;
        }

        filepath = argv[1];
        n = stoi(argv[2]);
    }

    map<string, int> key_cnts;
    map<pair<string, string>, int> ngram;
    map<string, int> unique_words;

    /* Form n-gram dataset */
    ifstream in(filepath);
    if (in.is_open())
    {
        string sentence;
        while (getline(in, sentence))
        {
            vector<string> words;
            split(sentence, words, ' ');

            vector<string> dataset;
            for (auto word : words)
            {
                unique_words[word]++;
                dataset.push_back(word);

                string key = "";

                for (int i = n; i > 1; i--)
                {
                    int index = dataset.size() - i;
                    if (index >= 0)
                    {
                        key += dataset[index];
                    }
                    else
                    {
                        key += "<s>";
                    }

                    if (i != 2)
                    {
                        key += " ";
                    }
                }
                key_cnts[key]++;
                pair<string, string> p{key, dataset[dataset.size()-1]};
                ngram[p]++;
            }
        }
        in.close();
    }
    else
    {
        cout << "Cannot open file " << filepath << endl;
        return -1;
    }

//    /* Calculate probability */
    vector<pair<pair<string, string>, double>> prob;
//    for (const auto& it : ngram)
//    {
//        double value = (double)it.second / (double)key_cnts[it.first.first];
//        pair<pair<string, string>, double> p{it.first, value};
//        prob.push_back(p);
//    }


    /* Witten-Bell */

    /* Write to file */
    string infilename = filepath.substr(0, filepath.find_last_of('.'));
    string outname = infilename;
    outname += "_" + to_string(n) + "-gram.csv";

    double kartez_size = unique_words.size() * unique_words.size();

    map<string, double> uni_pair_with;
    for (auto nkey : ngram)
    {
        uni_pair_with[nkey.first.first]++;
    }

    for (auto nkey : ngram)
    {
        double T = uni_pair_with[nkey.first.first];
//        double T = 0;
//        for (auto n : ngram)
//        {
//            if (nkey.first.first == n.first.first) T++;
//        }

        double pwb = nkey.second / (unique_words[nkey.first.first] + T);
        pair<pair<string, string>, double> p{nkey.first, pwb};
        prob.push_back(p);
    }

    for (auto uw1 : unique_words)
    {
        double T = uni_pair_with[uw1.first];
//        double T = 0;
//        for (auto nkey : ngram)
//        {
//            if (nkey.first.first == uw1.first) T++;
//        }

        pair<string, string> mypair{uw1.first, "*"};

        double Z = kartez_size - T;

        double pwb = T / (Z * (unique_words[mypair.first] + T));

        pair<pair<string, string>, double> p{mypair, pwb};
        prob.push_back(p);
    }

    /* Sort */
    sort(begin(prob), end(prob),
         [](const pair<pair<string, string>, double>& a,
            const pair<pair<string, string>, double>& b)
    {
        return a.second > b.second;
    });

    ofstream out(outname);
    if (out.is_open())
    {
        for (const auto& it : prob)
        out << "\"" << it.first.first << "\"" << ";"
            << "\"" << it.first.second << "\"" << ";" << it.second << ";" << endl;
        out.close();
    }
    else
    {
        cout << "Cannot write to file outname" << endl;
        return -1;
    }




//    cout << "First: " << prob[0].first.first << " " << prob[0].first.second << " "
//         << prob[0].second << endl;

//    cout << "Last: " << prob[prob.size() - 1].first.first << " " << prob[prob.size() - 1].first.second << " "
//         << prob[prob.size() - 1].second << endl;

//    /* Perplexity */
//    double entropy = 0;

//    for (const auto& it : prob)
//    {
////        entropy += -(unique_words[it.first.second] / unique_words.size()) * log2(it.second);
//        entropy += it.second * log2(it.second);
//    }
//    entropy *= -1;

//    double perplexity = pow(2, entropy);
//    cout << "entropy: " << entropy << endl << "perplexity: " << perplexity << endl;



    return 0;
}
